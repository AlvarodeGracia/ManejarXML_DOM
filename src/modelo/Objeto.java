package modelo;

public class Objeto {
	
	
	private String nombre;
	private String descripcion;
	private int cantidad;
	private float coste;
	
	public Objeto() {
		this.nombre = "";
		this.descripcion = "";
		this.cantidad = 0;
		this.coste = 0;
	}
	
	public Objeto(String nombre, String descripcion, int cantidad, float coste) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.cantidad = cantidad;
		this.coste = coste;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public float getCoste() {
		return coste;
	}

	public void setCoste(float coste) {
		this.coste = coste;
	}
	
	
	

}
