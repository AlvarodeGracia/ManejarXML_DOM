package modelo;

import java.util.ArrayList;

public class Personaje {
	
	private String id;
	private String nombre;
	private Clase clase;
	private boolean isEnvenenado;
	private ArrayList<Objeto> mochila;
	
	public Personaje() {
		
		this.id = "";
		this.nombre = "";
		this.clase = null;
		this.isEnvenenado = false;
		this.mochila = new ArrayList<Objeto>();
	}
	

	public Personaje(String id, String nombre, Clase clase, int vida, float oro, boolean isEnvenenado) {
		this.id = id;
		this.nombre = nombre;
		this.clase = clase;
		this.isEnvenenado = isEnvenenado;
		this.mochila = new ArrayList<Objeto>();
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Clase getClase() {
		return clase;
	}

	public void setClase(Clase clase) {
		this.clase = clase;
	}

	public boolean isEnvenenado() {
		return isEnvenenado;
	}

	public void setEnvenenado(boolean isEnvenenado) {
		this.isEnvenenado = isEnvenenado;
	}

	public ArrayList<Objeto> getMochila() {
		return mochila;
	}

	public void setMochila(ArrayList<Objeto> mochila) {
		this.mochila = mochila;
	}

}
