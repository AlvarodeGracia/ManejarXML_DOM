package xml_dom;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import modelo.Objeto;
import modelo.Personaje;

public class EscribirXML {
	
	
	//Este metodo guardara un array de objetos, puede estudiarse  e implementarse
	//para guardar cualquier tipo de arry de objetos en XML
	public void escribir(ArrayList<Personaje> personajes) {
		
		try {
			
			//Esto llama a una factoria de de contructor de Documentos DOM que es Singleton
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			
			//Este objeto es el encargado de generar documentos DOM, que son documentos XML
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			//Como queremos guardar un documento xml creado por nosotros
			//generamos un documento XML vacio
			Document doc = docBuilder.newDocument();
			
			//Ahora empezamos a crear el documento XML
			//Empezamos creando el elemento raiz
			Element rootElement = doc.createElement("personajes");
			
			//Ahora agregamos ese elemento al documento
			doc.appendChild(rootElement);
			
			//Recorremos el array que nos han dado, vamos  a ir creando
			//elementos personaje y agregandolos a la raiz <personajes> del documento
			for(Personaje personaje: personajes) {
				
				
				//Creamos el elemento principal, en este caso Personaje
				Element personaje_element = doc.createElement("personaje");
				//Agregamos el elemento a la raiz del documento
				rootElement.appendChild(personaje_element);
				
				//Ahora veremos como añadir ATRIBUTOS a las etiquetas HTML
				//en este caso tenemos un id
				Attr attr_id = doc.createAttribute("id");
				//Establecemos el valor del atributo como STRING
				attr_id.setValue(personaje.getId());
				//Agregamos el atributo ID al elemento personaje
				personaje_element.setAttributeNode(attr_id);
				
				//Ahora veremos como agregar un elemento dentro de otro elemento,
				//En este caso veremos como agregar el nombre del personaje
				//Creamos el elemento
				Element nombre_element = doc.createElement("nombre");
				//Creamos el texto que guardara el elemento
				Text nodeText_nombre = doc.createTextNode(personaje.getNombre());
				//agregamos el texto al elemento
				nombre_element.appendChild(nodeText_nombre);
				//Agregamos el elemento nombre, al elemento personaje
				personaje_element.appendChild(nombre_element);
				
				//Ahora veremos como guardar un objeto que esta dentro de otro objeto
				//En un document oXML es un elemento que tiene a su vez mas elementos
				//por lo que en el fondo ya lo hemos visto.
				Element clase_element = doc.createElement("clase");
				personaje_element.appendChild(clase_element);
				
				//Agregamos el nombre de la clase
				Element nombre_clase_element = doc.createElement("nombre");
				Text nodeText_nombre_clase = doc.createTextNode(personaje.getClase().getNombre());
				nombre_clase_element.appendChild(nodeText_nombre_clase);
				clase_element.appendChild(nombre_clase_element);
				
				//Agregamos el tipo de la clase
				//En este ejemplo nos slatamos una linea y no generamos un obejto Text
				//Si no que se lo ponemos directamente a la funcion
				Element tipo_clase_element = doc.createElement("tipo");
				tipo_clase_element.appendChild(doc.createTextNode(personaje.getClase().getTipo()));
				clase_element.appendChild(tipo_clase_element);
				
				
				//Ahora vamos a ver como agregar un array que esta dentro de un objeto
				
				//Creamos el elemento raiz del array donde se guardarn todos los elementos
				//de ese array
				Element mochile_element = doc.createElement("mochila");
				personaje_element.appendChild(mochile_element);
				
				for(Objeto obj: personaje.getMochila()) {
					
					Element objeto_element = doc.createElement("objeto");
					mochile_element.appendChild(objeto_element);
					
					
					Element nombre_objeto_element = doc.createElement("nombre");
					Text nodeText_nombre_objeto = doc.createTextNode(obj.getNombre());
					nombre_objeto_element.appendChild(nodeText_nombre_objeto);
					objeto_element.appendChild(nombre_objeto_element);
					
					Element descripcion_objeto_element = doc.createElement("descripcion");
					Text nodeText_descripcion_objeto = doc.createTextNode(obj.getDescripcion());
					descripcion_objeto_element.appendChild(nodeText_descripcion_objeto);
					objeto_element.appendChild(descripcion_objeto_element);
					
					//Ahora vamos a guardar un elemento que no es un STRING, por l oque para poder
					//guardarlo primero habra que convertiirl oa STRING
					Element cantidad_objeto_element = doc.createElement("cantidad");
					//Convertimos el int en STRING
					String cantidad = String.valueOf(obj.getCantidad());
					Text nodeText_cantidad_objeto = doc.createTextNode(cantidad);
					cantidad_objeto_element.appendChild(nodeText_cantidad_objeto);
					objeto_element.appendChild(cantidad_objeto_element);
					
					//Ahora hacemos lo mismo en un float suprimiendo un paso
					Element coste_objeto_element = doc.createElement("coste");
					Text nodeText_coste_objeto = doc.createTextNode(String.valueOf(obj.getCoste()));
					coste_objeto_element.appendChild(nodeText_coste_objeto);
					objeto_element.appendChild(coste_objeto_element);
					
				}//Continua el bucle agregando objetos del  arraylist hasta que se termine
				
				
				//Ahora terminaremos guardando un boolean
				Element envenenado_element = doc.createElement("envenendado");
				String bool = String.valueOf(personaje.isEnvenenado());
				Text nodeText_envenenado = doc.createTextNode(bool);
				envenenado_element.appendChild(nodeText_envenenado);
				personaje_element.appendChild(envenenado_element);
				
				//Ya tenemos el document ocreado ahora es hora de guardarlo
				
				//Esta clase se encargar de crear una factoria de Transformer
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				//Esta clase convierte una fuente XML (en este caso DOM)  a otro tipo de archivo como file
				//Puedes convertir objetos DOM a SAX por ejemplo
				Transformer transformer = transformerFactory.newTransformer();
				
				//Con este objeto preparamos el DOM que hemos creado para ser transformado
				DOMSource source = new DOMSource(doc);
				
				//Con este objeto preparamos a que vamos a convertir el XML
				//en este caso a fichero
				StreamResult result = new StreamResult(new File("Archivos/empeladoDOM.xml"));
			
				//Transfomamos el DOM a FILE  y lo gaurdamos
				transformer.transform(source, result);

				//Con esto ya tenemos una funcion que guarda un XML con DOM
				
			}
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
